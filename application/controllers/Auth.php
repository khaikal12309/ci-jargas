<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//load model admin
		$this->load->model('UserModel');
		// if (! $this->session->userdata()){
		// 	$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
		// 	redirect("auth");
		// }
		// if($this->session->userdata('state') == "siswa")
		// {
		// 	redirect("siswa/SiswaDashboard");
		// } elseif ($this->session->userdata('state') == "guru") {
		// 	redirect("guru/GuruDashboard");
		// } elseif ($this->session->userdata('state') == "admin") {
		// 	redirect("admin/AdminDashboard");
		// } else{
		// 	$this->session->set_flashdata('error', 'Anda tidak punya akses kesini');
		// 	redirect("auth");
		// }
	}

	public function index($msg=Null)
	{	
		$data_to_template = array(
			'url' => 'Daftar',
			'content' => $this->load->view('register',$msg, TRUE),
			'header' => $this->load->view('layouts/header', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);

		$this->load->view('layouts/template', $data_to_template);	
	}

	public function login()
	{	
		$data_to_template = array(
			'url' => 'Daftar',
			'content' => $this->load->view('login',null, TRUE),
			'header' => $this->load->view('layouts/header', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);

		$this->load->view('layouts/template', $data_to_template);	
	}

	public function do_register()
	{
		$code=0;
		if ($_POST) {
			$create = $this->UserModel->save();
			if ($create){
				$code=1;
				$msg='Registrasi berhasil, mohon tunggu verifikasi';
			} else{
				$msg = 'Email telah terdaftar';
			}
		}else{
			$msg = 'Gagal menyimpan data';
		}

		if ($code){
			$this->session->set_flashdata('success', $msg);
		}else{
			$this->session->set_flashdata('error', $msg);
		}
		redirect ('Auth/index') ;

	}

	public function do_login()
	{
		$code=0;
		$data['email'] = $_POST['email'];
		$data['password'] = md5($_POST['password']);
		$code = $this->UserModel->check_login($data);

		if($code)
		{
			$data = $code->row();
			$session_data = array(
				'id_user'   => $data->id,
				'name' => $data->name,
				'email' => $data->email,
				'state' => $data->state,
			);

			//set session userdata
			$this->session->set_userdata($session_data);
			if ($data->state == 'admin') 
			{
				redirect('admin/AdminDashboard');
			}
			if ($data->state == 'user') 
			{
				$this->session->set_flashdata('success', 'Selamat datang '.$data->name_user.'');
				redirect('Home');
			}
		}else{
			$this->session->set_flashdata('error', 'Email atau Password salah');
			$referred_from = $this->session->userdata('referred_from'); 
			redirect($referred_from, 'refresh');
		}
		redirect ('Auth/login') ;

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Home'); 
	}

	
}