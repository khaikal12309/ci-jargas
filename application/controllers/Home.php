<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$data_to_template = array(
			'url' => 'profile',
			'content' => $this->load->view('home',null, TRUE),
			'header' => $this->load->view('layouts/header', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);

		$this->load->view('layouts/template', $data_to_template);	
	}

	public function cek_tagihan()
	{	
		$data_to_template = array(
			'url' => 'profile',
			'content' => $this->load->view('cek_tagihan',null, TRUE),
			'header' => $this->load->view('layouts/header', null, TRUE),
			'footer' => $this->load->view('layouts/footer', null, TRUE),
		);

		$this->load->view('layouts/template', $data_to_template);	
	}

	public function do_cek_tagihan()
	{
		$username   = "0895342960174";
		$apiKey   = "488602bcb10ce2a6";
		$ref_id = "'.rand().'";
		$ref_id = rand(2,1000 		 );
		$signature  = md5($username.$apiKey.$ref_id);

		$json = '{
			"commands"	: "inq-pasca",
			"username"	: "0895342960174",
			"code"      : "PGAS",
			"hp"        : "'.$this->input->post('no_id').'",
			"ref_id":"'.$ref_id.'",
			"sign":"'.$signature.'"
		}';
		// print_r($json);
		// die();

		$url = "https://testpostpaid.mobilepulsa.net/api/v1/bill/check";

		$ch  = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$data = curl_exec($ch);
		curl_close($ch);

		$data = json_decode($data);

		echo json_encode($data);
	}

}
