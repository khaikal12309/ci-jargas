<aside id="left-panel" class="left-panel">
	<nav class="navbar navbar-expand-sm navbar-default">
		<div id="main-menu" class="main-menu collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active">
					<a href="index.html"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
				</li>
				<li class="menu-title">Master Data</li><!-- /.menu-title -->
				<li class="menu-item-has-children dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-users"></i> User</a>
					<ul class="sub-menu children dropdown-menu">                            
						<li><i class="fa fa-check-circle-o"></i><a href="<?php echo base_url().'index.php/admin/User/index/1'; ?>"> Aktif</a></li>

						<li><i class="fa fa-times-circle-o"></i><a href="<?php echo base_url();?>index.php/admin/User/index/0">Inactive</a></li>
					</ul>
				</li>

				<li class="menu-title">Transaction</li><!-- /.menu-title -->

				<li>
					<a href="widgets.html"> <i class="fa fa-money"></i> Pembayaran </a>
				</li>
				<li class="menu-title">Configuration</li><!-- /.menu-title -->
				<li>
					<a href="widgets.html"> <i class="fa fa-gear"></i> Sistem Configuration</a>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</nav>
</aside>